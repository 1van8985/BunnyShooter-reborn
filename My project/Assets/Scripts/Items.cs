using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    public enum ItemType
    {
        Blue, Yellow, Green,
    }

    public ItemType itemType;
    public int amount;
}
